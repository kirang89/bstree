(ns bstree.core)


(defrecord BSTreeNode [value left right])

(defn min-val [tree]
  "Returns minimum value of a tree."
  (loop [node tree]
    (if (nil? (:left node))
      (:value node)
      (recur (:left node)))))

(defn search [{:keys [value left right] :as tree} val]
  "Searches for a value in tree."
  (cond
    (or (nil? val) (nil? tree)) nil
    (= value val) tree
    (< val value) (search left val)
    (> val value) (search right val)
    :else nil))

(defn insert-value [{:keys [value left right] :as tree} val]
  "Inserts value into tree without balancing"
  (cond
    (nil? val)    tree
    (nil? tree)   (BSTreeNode. val nil nil)
    (< val value) (BSTreeNode. value (insert-value left val) right)
    (> val value) (BSTreeNode. value left (insert-value right val))
    :else         tree))

(defn delete-value [{:keys [value left right] :as tree} val]
  "Deletes value from tree without balancing."
  (cond
    (nil? val)    tree
    (nil? tree)   nil
    (< val value) (BSTreeNode. value (delete-value left val) right)
    (> val value) (BSTreeNode. value left (delete-value right val))
    (nil? left)   right
    (nil? right)  left
    :else         (let [min-value (min-val right)]
                    (BSTreeNode. min-value left (delete-value right min-value)))))

(defn height
  ([tree] (height tree 0))
  ([tree count]
   (if (nil? tree)
     count
     (max (height (:left tree) (inc count))
          (height (:right tree) (inc count))))))

(defn balance-factor [tree]
  (- (height (:left tree)) (height (:right tree))))

(defn balanced? [tree]
  (contains? #{-1 0 1} (balance-factor tree)))

(defn left-rotate [{:keys [value left right] :as tree}]
  (BSTreeNode. (:value right)
               (BSTreeNode. value left (:left right))
               (:right right)))

(defn right-rotate [{:keys [value left right] :as tree}]
  (BSTreeNode. (:value left)
               (:left left)
               (BSTreeNode. value (:right left) right)))

(defn left-left-case? [{:keys [value left] :as tree}]
  (> (balance-factor tree) 1))

(defn right-right-case? [{:keys [value right] :as tree}]
  (< (balance-factor tree) -1))

(defn left-right-case? [{:keys [value left] :as tree}]
  (and (left-left-case? tree) (< (balance-factor left) 0)))

(defn right-left-case? [{:keys [value right] :as tree}]
  (and (right-right-case? tree) (> (balance-factor right) 0)))

(defn rotate [{:keys [value left right] :as tree}]
  "Rotates tree based on node alignment."
  (cond
    (right-left-case? tree)  (left-rotate (BSTreeNode. value left (right-rotate right)))
    (left-right-case? tree)  (right-rotate (BSTreeNode. value (left-rotate left) right))
    (right-right-case? tree) (left-rotate tree)
    (left-left-case? tree)   (right-rotate tree)
    :else                    tree))

(defn balance [{:keys [value left right] :as tree}]
  "Returns a balanced tree."
  (cond
    (balanced? tree) tree
    (and (balanced? left) (balanced? right)) (rotate tree)
    :else (rotate (BSTreeNode. value (rotate left) (rotate right)))))

(def insert
  "Inserts value and balances tree."
  (comp balance insert-value))

(def delete
  "Deletes value and balances tree."
  (comp balance delete-value))

(defn ->tree [coll]
  "Returns a tree with values in collection inserted into it."
  (reduce #(insert %1 %2) nil coll))

(defn inorder-traverse [{:keys [value left right] :as tree}]
  "Traverses a tree inorder style."
  (if (nil? tree)
    []
    (concat (inorder-traverse left)
            [value]
            (inorder-traverse right))))
