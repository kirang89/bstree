(ns bstree.core-test
  (:require [clojure.test :refer :all]
            [bstree.core :refer :all]))


(def tree (->tree [1 2 3 4 5 6]))

(deftest inorder-traversal
  (testing "Inorder traversal"
    (is (= (inorder-traverse tree) [1 2 3 4 5 6]))
    (is (= (inorder-traverse (->tree [10 20 15 50 30])) [10 15 20 30 50]))))

(deftest search-val-at-root
  (testing "Search for value of root element"
    (is (.equals (search tree 4) tree))))

(deftest search-val-at-left
  (testing "Search for value located in left subtree"
    (is (not (nil? (search (:left tree) 1))))))

(deftest search-val-at-right
  (testing "Search for value located in right subtree"
    (is (not (nil? (search (:right tree) 6))))))

(deftest search-invalid-val
  (testing "Search for invalid value"
    (is (nil? (search tree 30)))))

(deftest search-nil-val
  (testing "Search for a `nil` value"
    (is (nil? (search tree nil)))))

(deftest search-val-in-invalid-tree
  (testing "Search for value in an invalid tree"
    (is (nil? (search nil 10)))))

(deftest insert-at-left-subtree
  (testing "Insert at left subtree"
    (is (= (inorder-traverse (insert-value tree 0)) [0 1 2 3 4 5 6]))))

(deftest insert-at-right-subtree
  (testing "Insert at right subtree"
    (is (= (inorder-traverse (insert-value tree 8)) [1 2 3 4 5 6 8]))))

(deftest insert-duplicate-value
  (testing "Insert a duplicate value"
    (is (.equals (insert tree 5) tree))))

(deftest insert-invalid-value
  (testing "Insert invalid value"
    (is (.equals (insert-value tree nil) tree))))

(deftest delete-value-left-subtree
  (testing "Delete value from left subtree"
    (is (= (inorder-traverse (delete tree 2)) [1 3 4 5 6]))))

(deftest delete-value-right-subtree
  (testing "Delete value from right subtree"
    (is (= (inorder-traverse (delete tree 5)) [1 2 3 4 6]))))

(deftest delete-value-invalid-tree
  (testing "Delete value from invalid tree"
    (is (nil? (delete-value nil 20)))))

(deftest delete-invalid-value
  (testing "Remove invalid value from tree"
    (is (.equals (delete tree 10) tree))))

(deftest left-rotation
  (testing "Left rotation for right-right case"
    (let [tree (reduce insert-value nil [10 20 30])]
      (is (.equals (left-rotate tree) (->tree [10 20 30]))))))

(deftest right-rotation
  (testing "Right rotation for left-left case"
    (let [tree (reduce insert-value nil [10 5 4])]
      (is (.equals (right-rotate tree) (->tree [ 10 5 4]))))))

(deftest left-right-rotation
  (testing "Double rotation for left-right case"
    (let [tree (reduce insert-value nil [10 5 8])]
      (is (.equals (rotate tree) (reduce insert-value nil [8 5 10]))))))

(deftest right-left-rotation
  (testing "Double rotation for right-left case"
    (let [tree (reduce insert-value nil [10 30 20])]
      (is (.equals (rotate tree) (reduce insert-value nil [20 10 30]))))))
