# bstree

A Self-Balancing Binary Search Tree in Clojure.

## Usage Examples

```clojure
;; Create a new tree
(def tree (->tree [1 2 3 4 5 6]))

;; Insert value
(insert tree 7)

;; Delete value
(delete tree 4)

;; Search for a value
(search tree 5)

;; Inorder traversal
(inorder-traverse tree)		;; [1 2 3 4 5 6 7]

```

## Running Tests

	$ lein test

## License

Copyright © 2017 Kiran Gangadharan

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
